package template.speedpointer;

import common.ListNode;

/**
 * @ClassName: Solution
 * @Description:
 * @author: wangz48
 * @date: 2021-12-28 10:14
 */
public class Solution {
    /**
     * 判断链表是否有环
     * @param head
     * @return boolean
     */
    public boolean hasCycle(ListNode head){
        //1.定义快慢指针
        ListNode fast,slow;
        //2.初始化快慢指针
        fast = slow = head;
        //3.循环
        while (fast != null && fast.next != null){
            //3.1 移动快指针
            fast = fast.next.next;
            //3.2 移动慢指针
            slow = slow.next;
            //3.3 相遇操作
            if (slow == fast){
                return true;
            }
        }
        return false;
    }
}
