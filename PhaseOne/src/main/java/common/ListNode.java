package common;

/**
 * @ClassName: ListNode
 * @Description: ListNode
 * @author: wangz48
 * @date: 2021-12-28 10:12
 */

public class ListNode {
    public int val;
    public ListNode next;

    public ListNode(int val, ListNode next) {
        this.val = val;
        this.next = next;
    }
}
