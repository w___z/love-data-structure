package 链式结构.linkedlist;

import 链式结构.List;

/**
 * @ClassName: Main
 * @Description:
 * @author: wangz48
 * @date: 2021-12-27 10:59
 */
public class Main {
    public static void main(String[] args) {
        List<Integer> list = new LinkedList<>();
        list.add(20);
        list.add(30);
        list.add(60);
        list.add(3,50);
        list.add(66);
        list.remove(4);
        System.out.println(list.toString());
    }
}
