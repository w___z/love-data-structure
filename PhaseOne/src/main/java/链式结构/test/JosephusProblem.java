package 链式结构.test;

/**
 * @ClassName: JosephusProblem
 * @Description: 约瑟夫问题
 * @author: wangz48
 * @date: 2021-12-29 9:46
 */

public class JosephusProblem {
    public static void main(String[] args) {
       josephus(3,8);
    }

    /**
     * @param m 数到几淘汰
     * @param people 共有几个人
     */
    public static void josephus(int m,int people){
        CircleLinkedList<Integer> list = new CircleLinkedList<>();
        for (int i = 1; i <= people; i++) {
            list.add(i);
        }
        System.out.println(list.toString());
        //指向头结点
        list.reset();
        //循环杀
        while(! list.isEmpty()){
            for (int i = 1; i < m; i++) {
                list.next();
            }
            System.out.println(list.remove());
        }
    }
}
