package 链式结构.arraylist;


/**
 * @ClassName: ArrayList
 * @Description: 自定义Arraylist
 * @author: wangz
 * @date: 2021-12-24 15:00
 */
public class ArrayList<E> {
    /**
     * 动态数组成员变量的设计
     * 1.size 元素的数量
     * 2.elements 用于存储元素
     * 3.DEFAULT_CAPACITY 默认容量
     */
    private int size;
    private E[] elements;
    private static final int DEFAULT_CAPACITY = 10;
    private static final int ELEMENT_NOT_FOUND = -1;

   /*===========构造方法================*/

    public ArrayList(){
        this(DEFAULT_CAPACITY);
    }
    public ArrayList(int capaticy){
        capaticy = (capaticy < DEFAULT_CAPACITY ? DEFAULT_CAPACITY : capaticy);
        elements = (E[]) new Object[capaticy];
    }

    /*===========成员方法===============*/

    /**
     * 元素的数量
     */
    public int size(){
        return size;
    }
    /**
     * 判断是否为空
     */
    public boolean isEmpty(){
        return size == 0;
    }
    /**
     * 获取index位置的元素
     * @return
     */
    public E get(int index){
        rangeCheck(index);
        return elements[index];
    }
    /**
     * 设置新元素，返回原来的元素
     */
    public E set(int index, E element){
            rangeCheck(index);
            E old = elements[index];
            elements[index] = element;
            return  old;
    }
    /**
     * 遍历元素,返回下标
     */
    public int indexOf(E element){
        if (element == null){
            for (int i = 0; i <size ; i++) {
                if (elements[i] == null){return i;}
            }
        }else {
            for (int i = 0; i < size; i++) {
                if (elements[i].equals(element)) {
                    return i;
                }
            }
        }
        return ELEMENT_NOT_FOUND;
    }

    /**
     * contains ，是否包含某个元素
     */
    public boolean contains(E element){
        return indexOf(element) != ELEMENT_NOT_FOUND;
    }

    /**
     * 清除所有元素
     */
    public void clear(){
//       //只要size=0别人就拿不到其中的元素了(对使用者来说已清除)
//        size = 0;
        for (int i = 0; i < size; i++) {
            elements[i] = null; //销毁数组元素与对象的引用
        }
        size = 0;
    }

    /**
     * 新增元素
     * 往size的位置放元素
     * @param element
     */
    public void add(E element){
//        elements[size++] = element;
    add(size,element);
    }

    /**
     * 打印
     */
    @Override
    public String toString(){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("size=")
                .append(size)
                .append(",[");
        for (int i = 0; i < size; i++) {
            if (i != 0){
                stringBuilder.append(",");
            }
            stringBuilder.append(elements[i]);
        }
        stringBuilder.append("}");
        return stringBuilder.toString();
    }

    /**
     * 删除元素
     * 删除当前下标的元素 == 把它之后的元素往前移动，size--,最后设置为null
     * @param index
     * @return elements[index]
     */
    public E remove(int index){
        E old = elements[index];
       rangeCheck(index);
        for (int i = index+1; i < size; i++) {
            elements[i-1] = elements[i];
        }
        size--;
        elements[size] = null;
        return old;
    }

    /**
     * 添加元素
     * 让当前元素的值==element、后面的元素往后移动、size--
     * @param index
     * @param element
     */
    public void add(int index,E element){
        rangeCheckForAdd(index);
        ensureCapacity(size+1);
        for (int i = size-1; i >= index; i--) {
            elements[i+1] = elements[i];
        }
        elements[index] = element;
        size++;
    }


    /*================保证容量================*/

    /**
     * 保证要有capacity的容量(elements数组的长度)
     * @param capacity
     */
    private void ensureCapacity(int capacity){
        int oldCapacity = elements.length;
        //如果现在容量够，则不用扩容
        if(oldCapacity >= capacity) {return;}
        else {
            //如果不够，则扩容。 *1.5
            int newCapacity = oldCapacity + (oldCapacity >> 1);
            System.out.println("扩容后长度：" + newCapacity);
            E[] newElements = (E[]) new Object[newCapacity];
            //移动元素
            for (int i = 0; i < size; i++) {
                newElements[i] = elements[i];
            }
            //更换引用
            elements = newElements;
        }
    }

    /**
     * 缩容
     */
    private void trim(){
        int oldCapacity = elements.length;
        int newCapacity = oldCapacity >> 1;
        if (size >= (newCapacity) || oldCapacity <= DEFAULT_CAPACITY){
            return;
        }
        //剩余空间很多——缩容
        E[] newElements = (E[]) new Object[newCapacity];
        for (int i = 0; i < size; i++) {
            newElements[i] = elements[i];
        }
        elements = newElements;

    }

    /*================异常封装=================*/

    private void outOfBoundsException(int index){
        throw new IndexOutOfBoundsException("Index"+index+"，Size"+size);
    }
    private void rangeCheck(int index){
        if (index < 0 || index >= size){
            outOfBoundsException(index);
        }
    }
    private void rangeCheckForAdd(int index){
        if (index < 0 || index > size){
            outOfBoundsException(index);
        }
    }
}
