package 链式结构.arraylist;

import lombok.Data;

/**
 * @ClassName: Main
 * @Description: 用于测试ArrayList
 * @author: wangz48
 * @date: 2021-12-24 15:01
 */
public class Main {
    public static void main(String[] args) {
        ArrayList<Integer> arrayList = new ArrayList();
        arrayList.add(99);
        arrayList.add(88);
        arrayList.add(77);
        arrayList.add(66);
        arrayList.add(55);
        arrayList.add(44);
        arrayList.clear();
        arrayList.add(arrayList.size(),100);
        arrayList.add(99);
        arrayList.add(99);
        arrayList.add(99);
        arrayList.add(66);
        System.out.println(arrayList.indexOf(66));
        arrayList.remove(3);
        System.out.println(arrayList.toString());

    }
}
