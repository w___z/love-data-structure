package 链式结构.栈和队列;

import java.util.LinkedList;

/**
 * @ClassName: Deque
 * @Description: 双端队列
 * @author: wangz48
 * @date: 2021-12-30 11:22
 */
public class Deque<E> {
    private LinkedList<E> linkedList = new LinkedList();
    /**
     * 队列大小
     */
    public int size(){
        return linkedList.size();
    }
    /**
     * 队列是否为空
     */
    public boolean isEmpty(){
        return linkedList.isEmpty();
    }

}
