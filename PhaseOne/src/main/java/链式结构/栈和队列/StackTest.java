package 链式结构.栈和队列;

/**
 * @ClassName: StackTest
 * @Description:
 * @author: wangz48
 * @date: 2021-12-29 14:39
 */
public class StackTest {
    public static void main(String[] args) {
        Stack<Integer> stack = new Stack<>();
        stack.push(6);
        stack.push(7);
        stack.push(8);
        stack.push(9);
        stack.push(1);

        while(!stack.isEmpty()){
            System.out.println(stack.pop());
        }
    }
}
