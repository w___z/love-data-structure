package 链式结构.栈和队列;


import java.util.LinkedList;

/**
 * @ClassName: Queue
 * @Description: 队列
 * @author: wangz48
 * @date: 2021-12-29 16:26
 */
public class Queue<E> {
    private LinkedList<E> linkedList = new LinkedList();

    /**
     * 队列的长度
     * @return
     */
    public int size(){
       return linkedList.size();
    }

    /**
     * 判断是否为空
     * @return
     */
    public boolean isEmpty(){
        return linkedList.isEmpty();
    }

    /**
     * 尾插
     * @param element
     */
    public void offer(E element){
        linkedList.add(element);
    }

    /**
     * 头删
     */
    public E poll(){
       return linkedList.poll();
    }

    /**
     * 获取队头
     * @return
     */
    public E peek(){
        return linkedList.get(0);
    }
}
