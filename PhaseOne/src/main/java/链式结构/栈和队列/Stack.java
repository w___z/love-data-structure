package 链式结构.栈和队列;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName: Stack
 * @Description: 栈的写法
 * @author: wangz48
 * @date: 2021-12-29 14:26
 */
public class Stack<E> {

    /**
     * 用来装数据
     */
    private List<E> list = new ArrayList<>();

    /**
     * 栈的大小
     */
    public int size(){
        return list.size();
    }

    /**
     * 栈是否为空
     */
    public boolean isEmpty(){
        return list.isEmpty();
    }

    /**
     * 入栈
     */
    public void push(E element){
        list.add(element);
    }

    /**
     * 出栈
     */
    public E pop(){
        return list.remove(size()-1);
    }

    /**
     * 查看栈顶元素
     */
    public E top(){
        return list.get(size()-1);
    }

}
