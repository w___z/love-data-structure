package 链式结构.栈和队列.test;

import java.util.Stack;

/**
 * @ClassName: StackToQueue
 * @Description: 栈实现队列
 * @author: wangz48
 * @date: 2021-12-30 10:36
 */

public class StackToQueue {
    /**
     * 相当于对头的栈
     */
    private Stack<Integer> queueHead ;
    /**
     * 相当于队尾的栈
     */
    private Stack<Integer> queueTail ;
    public StackToQueue() {
        queueHead = new Stack<>();
        queueTail = new Stack<>();
    }

    public void push(int x) {
        queueTail.push(x);
    }

    public int pop() {
        if (queueHead.empty()) {
            while (!queueTail.empty()) {
                Integer e = queueTail.pop();
                queueHead.push(e);
            }
        }
        return queueHead.pop();
    }

    public int peek() {
        if (queueHead.empty()) {
            while (!queueTail.empty()) {
                Integer e = queueTail.pop();
                queueHead.push(e);
            }
        }
        return queueHead.peek();
    }

    public boolean empty() {
        return queueHead.isEmpty() && queueTail.isEmpty();
    }
}
