package 链式结构.栈和队列;

/**
 * @ClassName: QueueTest
 * @Description:
 * @author: wangz48
 * @date: 2021-12-30 10:13
 */
public class QueueTest {
    public static void main(String[] args) {
        Queue<Integer> queue = new Queue();
        queue.offer(99);
        queue.offer(88);
        queue.offer(77);
        System.out.println(queue.peek());
        while (!queue.isEmpty()){
            System.out.println(queue.poll());
        }
    }
}
