package 动态规划;

/**
 * @ClassName: PaLouTi
 * @Description: 爬楼梯问题
 * @author: WangZe
 * @date: 2022/3/14 13:29
 */
public class PaLouTi {
    /**
     * 共有n阶楼梯，每次可以走一步或两步，问n阶楼梯有多少中走法
     */
    public static void main(String[] args) {
        PaLouTi paLouTi = new PaLouTi();
        System.out.println(paLouTi.solution(5));
    }
    public int solution(int n){
        if(n <= 2){return n;}
        return solution(n-1)+solution(n-2);
    }
}
