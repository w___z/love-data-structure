package sort;

import java.lang.reflect.GenericDeclaration;
import java.util.Arrays;

/**
 * @ClassName: Maopao
 * @Description: 冒泡排序
 * @author: WangZe
 * @date: 2022/3/13 10:51
 */
public class Maopao {
    public static void main(String[] args) {
        int[] arr = new int[]{2,3,4,6,7,5,90,23};
        Maopao maopao = new Maopao();
        maopao.bubbleSort(arr);
        System.out.println(Arrays.toString(arr));
    }

    public void bubbleSort(int[] arr){
        int n = arr.length;
        for (int end = n; end > 0; end--) {
            //添加布尔值，是否已经有序
            boolean isSorted = true;
            //每一次把最大的冒泡到最后面
            for (int begin = 1; begin < end; begin++) {
                if(arr[begin] < arr[begin-1]){
                    int tmp = arr[begin];
                    arr[begin] = arr[begin-1];
                    arr[begin-1] = tmp;
                    isSorted = false;
                }
            }
            //如果已经有序，直接退出
            if(isSorted){
                break;
            }
        }

    }
}
